# Arduino
1. Download the arduino IDE from: [https://www.arduino.cc/en/software](https://www.arduino.cc/en/software)
2. Open the arduino IDE
3. In the arduino IDE go to Tools/Manage Libraries.../
4. In the Library Manager search bar write firmata
5. Install "Firmata" (usually the first option)
6. Go back to the IDE
7. Go to File/Examples/Firmata and select StandardFirmata
8. A new arduino IDE window will open with the StandardFirmata code
9. Go to Tools/Board: and select your microcontroller
8. Still in Tools go to port and select your device
10. Click on the button with an arrow pointing to the right (->) to upload the code to your microcontroller. 

# Pure Data
1. Download the Pure Data IDE from: [http://msp.ucsd.edu/software.html](http://msp.ucsd.edu/software.html)
2. Open Pure Data
3. Go to Help/Find externals
4. In the externals finder' search bar look for: pduino
5. Click on the first option to install it
6. Still in external finder look for: comport
7. Click on the first option to install it
8. Close the external finder
9. Go back to Pure Data's main window
10. Go to File/Open and look for today's lab folder
11. From the blip_blink folder select the _main.pd file to open it.
12. The Pd patch will open in a new window
13. Follow the instructions inside the patch  

# Amplifying a piezo element signal
In for our piezo element to have a greater sensitivity and capture signals tthat happen on the surface it is placed on with need to use a preamp. Here is a small guide on how to build a piezo element preamp: [https://www.chair.audio/diy-piezo-preamp/](https://www.chair.audio/diy-piezo-preamp/) 
