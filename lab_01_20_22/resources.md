# Arduino
- [Arduino reference](https://www.arduino.cc/reference/en/)
- [Arduino tutorials](https://docs.arduino.cc/tutorials/)
- [Arduino forum](https://forum.arduino.cc/)
- [Arduino simulator](https://wokwi.com/)
- Arduinos at Väre Mechatronics workshop: Contact [janne.ojala@aalto.fi](mailto:janne.ojala@aalto.fi)
- Electronics in Helsinki:
	- [Partco](https://www.partco.fi/en/)
	- [Bebek](http://www.bebek.fi/)
	- [Ural Tone](https://en.uraltone.com/)

# Pure Data
- [Pure Data online manual](http://msp.ucsd.edu/Pd_documentation/index.htm)
- [Pure Data extensive manual](http://aspress.co.uk/ds/pdf/pd_intro.pdf) 
- [Pure Data FLOSS manual](http://archive.flossmanuals.net/pure-data/)
- [Pure Data forum](https://forum.pdpatchrepo.info/)

# Other platforms of interest
- [Bela board](https://bela.io/)
- [Teensy](https://www.pjrc.com/teensy/)
- [ESP32](https://www.espressif.com/en/products/devkits)
