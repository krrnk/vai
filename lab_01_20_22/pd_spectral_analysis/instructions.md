# Installation 
1. Download the Pure Data IDE from: [http://msp.ucsd.edu/software.html](http://msp.ucsd.edu/software.html)
2. Open Pure Data
3. Go to Help/Find externals
4. In the externals finder' search bar look for: timbreID
5. Click on the first option to install it
8. Close the external finder
9. Go back to Pure Data's main window
10. Go to File/Open and look for today's lab folder
11. From the pd_spectral_analysis folder select the audio-features-RT.pd file to open it.
12. The Pd patch will open in a new window
13. Follow the instructions inside the patch
14. Explore more possibilities of timbreID by downloading the [examples folder](http://williambrent.conflations.com/pd/timbreID/timbreID-examples.zip)
 
