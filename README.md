# README #

### What is this repository for? ###

* Examples and exercises for the Voice and Auditory Interaction course

### How do I get set up? ###

* Dependencies
	- [Pure Data](http://msp.ucsd.edu/software.html) 
	- [Arduino](https://www.arduino.cc/en/software)
* Installation

	`git clone https://krrnk@bitbucket.org/krrnk/vai.git`
	
	or
	
	Download and unzip [this folder](https://bitbucket.org/krrnk/vai/get/39424c70a72b.zip)